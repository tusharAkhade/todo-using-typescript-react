import { FC, useState, ChangeEvent } from "react";
import "./App.css";
import Todo from "./compoonents/Todo";
import { ITodo } from "./Interfaces";

const App: FC = () => {
  const [todo, setTodo] = useState<string>("");
  const [todoList, setTodoList] = useState<ITodo[]>([]);

  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setTodo(event.target.value);
  };

  const addTask = (): void => {
    const newTask = { taskName: todo };
    setTodoList([...todoList, newTask]);
    setTodo("");
  };

  const completeTask = (taskNameToDelete: string): void => {
    setTodoList(
      todoList.filter((todo) => {
        return todo.taskName !== taskNameToDelete;
      })
    );
  };

  return (
    <div className="App">
      <div className="header">
        <div className="inputContainer">
          <input
            type="text"
            placeholder="task..."
            value={todo}
            onChange={handleChange}
          />
        </div>
        <button onClick={addTask}>Add Todo</button>
      </div>

      <div className="todoList">
        {todoList.map((todo: ITodo, index: number) => {
          return <Todo key={index} todo={todo} completeTask={completeTask} />;
        })}
      </div>
    </div>
  );
};

export default App;
