import React from "react";
import { ITodo } from "../Interfaces";

interface TodoProps {
  todo: ITodo;
  completeTask(taskNameToDelete: string): void;
}

const Todo = ({ todo, completeTask }: TodoProps) => {
  return (
    <div className="todo">
      <div className="content">
        <span>{todo.taskName}</span>
      </div>
      <button onClick={() => completeTask(todo.taskName)}>X</button>
    </div>
  );
};

export default Todo;
